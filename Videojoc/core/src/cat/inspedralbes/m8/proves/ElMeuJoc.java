package cat.inspedralbes.m8.proves;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.Application;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Random;

public class ElMeuJoc extends ApplicationAdapter {
	SpriteBatch batch;
	
	// Constants
	final int TEMPS_ENTRE_APARICIO_ENEMIC_EN_SEGONS = 1;
	// Model de dades
	
	//Cada entitat del joc ha de tenir una textura i alguna manera de guardar la informacio de posicio i mida. La manera mes facil es amb un Rectangle
	Rectangle player;
	
	List<Rectangle> enemics;
	List<Rectangle> disparsJugador;
	float tempsUltimEnemic = 0; // En aquesta variable acumulem el temps passat des de lultima aparicio d'enemic
	//Grafics
	Texture playerTexture;
	Texture enemicTexture;
	Texture disparTexture;
	
	//S'executa una sola vegada al principi
	@Override
	public void create () {		
		batch = new SpriteBatch();
		
		//El rectangle es qui te la informacio de la posicio i mida del player
		//Les coordenades x i y son les del punt inferior a l'esquerra
		player = new Rectangle(0, 0, Constants.AMPLE_PLAYER, Constants.ALT_PLAYER);		
		//Inicialment la llista d'enemics esta buida
		enemics = new ArrayList<Rectangle>();
		disparsJugador= new ArrayList<Rectangle>();
		
		//Es equivalent al que ens diu el tutorial oficial amb Gdx.files...
		playerTexture = new Texture(Gdx.files.internal("naus/player.png"));
		enemicTexture = new Texture(Gdx.files.internal("naus/enemy.png"));
		disparTexture = new Texture(Gdx.files.internal("lasers/laserBlue.png"));
	
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
	}

	//S'executa continuament quan a libgdx li dona la gana
	@Override
	public void render () {
		// Neteja pantalla
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		// 1. Gestio input
		gestionarInput();
		// 2. Calculs enemics
		actualitza();
		// 3. Dibuixat
		batch.begin();
		//Es dibuixa la textura en la posicio del player i s'estira tota la seva amplaria i alcada
		batch.draw(playerTexture,player.x, player.y, player.width, player.height);
		//Dibuixem els enemics. Si no n'hi ha cap, no es dibuixa res.
		for (Rectangle enemic : enemics) {
			batch.draw(enemicTexture, enemic.x, enemic.y, enemic.width, enemic.height);
		}
		
		for (Rectangle dispar : disparsJugador) {
			batch.draw(disparTexture, dispar.x, dispar.y, dispar.width, dispar.height);
		}
		
		batch.end();
	}
	
	private void actualitza() {
		//1. Si ha passat un cert temps apareix un enemic
		// Obtenim el temps que ha passat des de l'ultim dibuixat de pantalla. Es el temps d'un frame.
		float delta = Gdx.graphics.getDeltaTime();
		//L'acumulem al temps per controlar si ha d'apareixer un enemic
		// Nou enemic
		tempsUltimEnemic += delta;
		if (tempsUltimEnemic > Constants.TEMPS_ENTRE_APARICIO_ENEMIC_EN_SEGONS) {
			int x = new Random().nextInt(300);
			int y=800;
			Rectangle nouEnemic = new Rectangle(x, y, Constants.AMPLE_ENEMIC, Constants.ALT_ENEMIC);
			enemics.add(nouEnemic);
			
			tempsUltimEnemic=0; //Molt important resetejar el comptador
		}
		
		for (Rectangle dispar : disparsJugador) {
			//volem que els dispars vagin cap amunt. Per tant sumem a la y
			dispar.y += Constants.VELOCITAT_DISPAR_PLAYER * delta;
		}
		
		//2. Cada enemic baixa un poc. Es fa modificant la seva y
		for (Rectangle enemic : enemics) {
			enemic.y -= Constants.VELOCITAT_NAU_ENEMIC * delta;
		}
		
		//Mirem si algun enemic esta xocant amb el player
		//Les colisions entre rectangles podrien complicar-se un poc
		//Afortunadament, hi ha un metode que diu si dos rectangles es solapen
		for (Rectangle enemic : enemics) {
			if(enemic.overlaps(player)) {
				Gdx.app.exit();
			}
		}
		
		for (Iterator iterEnemics = enemics.iterator(); iterEnemics.hasNext();) {
			Rectangle enemic = (Rectangle) iterEnemics.next();
			for (Iterator iterDispar = disparsJugador.iterator(); iterDispar.hasNext();) {
				Rectangle dispar = (Rectangle) iterDispar.next();
				if(enemic.overlaps(dispar)) {
					iterDispar.remove();
					iterEnemics.remove();
					break;
				}
			}
		}
		
		Gdx.app.debug("Enemics", "Hi ha " + enemics.size() + " enemics");
		
		//Els enemics que surten de la pantalla per baix, son eliminats de la List
		//Eliminar d'una llista mentre s'esta recorrent no es pot fer amb un for normal.
		//Una manera es amb iterators
		for (Iterator iterator = enemics.iterator(); iterator.hasNext();) {
			Rectangle enemic = (Rectangle) iterator.next();
			// Detectar quan surt per baix ha de tenir en compte l'alt de la nau
			if (enemic.y < -enemic.height) {
				iterator.remove();
				Gdx.app.debug("Enemics", "Elimino");
			}
		}
		Gdx.app.debug("Enemics", "Hi ha " + enemics.size() + " enemics");
	}
	
	private void gestionarInput() {
		
		float delta = Gdx.graphics.getDeltaTime();
		
		if (Gdx.input.isKeyPressed(Keys.LEFT)) {
			player.x -= Constants.VELOCITAT_NAU_JUGADOR*delta;
		}
		if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
			player.x += Constants.VELOCITAT_NAU_JUGADOR*delta;
		}
		// Per controlar que el player no surti de la pantalla s'ha d'evitar que la seva
		// coordenada x sigui inferior a 0 i que sigui superior a la mida de la pantalla
		// menys el seu ample		
		if (Gdx.input.isKeyPressed(Keys.UP)) {
			player.y += Constants.VELOCITAT_NAU_JUGADOR*delta;
		}
		if (Gdx.input.isKeyPressed(Keys.DOWN)) {
			player.y -= Constants.VELOCITAT_NAU_JUGADOR*delta;
		}
		
		if (Gdx.input.isKeyJustPressed(Keys.SPACE)) {
			// Fem que el dispar surti de la part central de dalt de la nau
			float x = player.x + player.width / 2;
			float y = player.y + player.height; // Recordem que la y es el punt de baix a lesquerra.
			disparsJugador.add(new Rectangle(x, y, Constants.AMPLE_LASER, Constants.ALT_LASER));
		}
		
		// Consultem la mida de la pantalla en pixels
		int amplePantalla = Gdx.graphics.getWidth();
		int altPantalla = Gdx.graphics.getHeight();
		
		// Comprovacio xoc esquerre
		if (player.x < 0) {
			player.x = 0;
		}
		
		// Comprovacio xoc dret
		if (player.x > (amplePantalla - player.width)) {
			player.x = amplePantalla - player.width;
		}
		
		// Comprovacio xoc esquerre
		if (player.y < 0) {
			player.y = 0;
		}
		
		// Comprovacio xoc dret
		if (player.y > altPantalla - player.height) {
			player.y = altPantalla - player.height;
		}
		
	}
	
	//S'executa una vegada al final
	@Override
	public void dispose () {
		batch.dispose();
		playerTexture.dispose();
		enemicTexture.dispose();
	}
}
