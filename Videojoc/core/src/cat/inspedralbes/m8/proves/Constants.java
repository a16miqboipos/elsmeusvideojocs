package cat.inspedralbes.m8.proves;

public class Constants {

	public static final int TEMPS_ENTRE_APARICIO_ENEMIC_EN_SEGONS = 1;
	public static final int VELOCITAT_NAU_JUGADOR = 200; // Pixels per segons
	public static final int VELOCITAT_NAU_ENEMIC = 150; // Pixels per segons
	public static final float AMPLE_LASER = 20;
	public static final float ALT_LASER = 100;
	public static final float VELOCITAT_DISPAR_PLAYER = 300;
	public static final float AMPLE_ENEMIC=100;
	public static final float ALT_ENEMIC=100;
	public static final float AMPLE_PLAYER=100;
	public static final float ALT_PLAYER=100;
}
