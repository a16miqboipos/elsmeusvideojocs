package cat.inspedralbes.m8.proves.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import cat.inspedralbes.m8.proves.ElMeuJoc;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		
		config.title = "Joc de Naus";
		config.width = 800;
		config.height = 480;
		
		new LwjglApplication(new ElMeuJoc(), config);
	}
}
